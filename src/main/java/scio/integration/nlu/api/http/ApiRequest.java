package scio.integration.nlu.api.http;

import static spark.Spark.before;
import static spark.Spark.halt;
import static spark.Spark.path;
import static spark.Spark.port;
import static spark.Spark.post;

import java.net.URI;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.util.Strings;

import com.calldesk.common.config.Configuration;
import com.calldesk.common.format.JsonParser;
import com.google.gson.JsonObject;

import scio.integration.nlu.api.security.Authenticate;
import spark.Request;

public class ApiRequest {
	private static Logger log = LogManager.getLogger("ApiRequest");

	public ApiRequest() {
		port(9999);

		path("/api", () -> {
			
			path("/dialogflow", () -> {
				before("/*", (q, a) -> 
				{
					boolean authorized = verifyID(q);
					if(!authorized) {
						log.warn("Unauthorized access via "+q.userAgent() + " - "+q.ip());
						JsonObject obj = new JsonObject();
						obj.addProperty("fail", "Key not found or invalid");
						a.type("application/json");
						halt(403, obj.toString());
					}
				});
				post("/intent-detect", DetectIntentApi.detectIntent);
				post("/intent-knowbase-detect", DetectIntentApi.detectIntentKnow);
			});

			path("/rasa-nlu", () -> {
				post("/intent-detect", DetectIntentApi.detectIntentRasa);
			});
		});
	}

	public static void main(String[] args) {
		try {
			Configuration.load("conf/project.properties");
		} catch (Exception e) {
			log.catching(e);
		}
		
		configureLogger();
		new ApiRequest();
	}
	/**
	 * This method setup Log4j.
	 */
	private static void configureLogger() {
		// Load Log4j configuration
		Properties properties = Configuration.prop;
		URI configLocation = URI.create(properties.getProperty("log4j2.configuration"));
		LoggerContext context = (LoggerContext) LogManager.getContext(false);

		context.setConfigLocation(configLocation);

		log = LogManager.getRootLogger();
	}

	private static boolean verifyID(Request q) {
		JsonObject jsonParam = JsonParser.parse(q.body());
		String keyID = JsonParser.get(jsonParam, "key", "");
		if(Strings.isBlank(keyID))
			return false;
		
		String pathToKey = Authenticate.getInstance().isAutherized(keyID);
		if(Strings.isBlank(pathToKey))
			return false;
		q.attribute("google_credentials_path", pathToKey);
		return true;
	}
}
