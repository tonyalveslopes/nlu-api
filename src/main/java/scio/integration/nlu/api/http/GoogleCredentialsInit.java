package scio.integration.nlu.api.http;

import java.io.FileInputStream;
import java.io.IOException;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.dialogflow.v2.SessionEntityTypesSettings;
import com.google.cloud.dialogflow.v2.SessionsSettings;
import com.google.common.collect.Lists;

public class GoogleCredentialsInit {
	private GoogleCredentialsInit() {
	}
	public static SessionsSettings authExplicit(String jsonPath, String type) throws IOException {
		// You can specify a credential file by providing a path to GoogleCredentials.
		// Otherwise credentials are read from the GOOGLE_APPLICATION_CREDENTIALS
		// environment variable.
		GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath))
				.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
		return SessionsSettings.newBuilder()
				
				.setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
		
		
	}
	
	public static SessionEntityTypesSettings authExplicit2(String jsonPath, String type) throws IOException {
		GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath))
				.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
		
		return SessionEntityTypesSettings.newBuilder().setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
	}
	
	public static com.google.cloud.dialogflow.v2beta1.SessionsSettings authExplicitBeta(String jsonPath) throws IOException {
		// You can specify a credential file by providing a path to GoogleCredentials.
		// Otherwise credentials are read from the GOOGLE_APPLICATION_CREDENTIALS
		// environment variable.
		GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath))
				.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));

		return com.google.cloud.dialogflow.v2beta1.SessionsSettings.newBuilder()
				.setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
	}
}
