package scio.integration.nlu.api.http;

import static spark.Spark.halt;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import scio.integration.nlu.api.model.RequestPayload;
import scio.integration.nlu.api.query.dialogflow.DetectIntentKnowledge;
import scio.integration.nlu.api.query.dialogflow.DetectIntentTexts;
import scio.integration.nlu.api.query.rasa.DetectTextRasa;
import spark.Request;
import spark.Response;
import spark.Route;

public class DetectIntentApi {
	private static Logger log = LogManager.getLogger("DetectIntentApi");

	private static final String PARAMS_INVALIDS =  "Params are not valids.";
	private DetectIntentApi() {
	}
	
	protected static Route detectIntent = new Route() {
		@Override
		public String handle(Request request, Response response) throws Exception {
			return detectIntent(request, response);
		}
		
		private String detectIntent(Request request, Response response) {
			response.type("application/json");
			DetectIntentTexts dt = new DetectIntentTexts();
			ObjectMapper mapper = new ObjectMapper();
			RequestPayload payload = null;
			try {
				payload = mapper.readValue(request.body(), RequestPayload.class);
			} catch (IOException e1) {
				log.catching(e1);
				parseFailResponse(response, e1.getMessage());
				return null;
			}

			if (payload == null || !payload.isValid()) {
				parseFailResponse(response, PARAMS_INVALIDS);
				return null;
			}
			
			String result = null;
			String pathToKey = request.attribute("google_credentials_path");
			try {
				result = dt.detectIntentTexts(payload.getProject_id(), payload.getText(), payload.getSession_id(),
						payload.getLang(), pathToKey);
			} catch (Exception e) {
				parseFailResponse(response, e.getMessage());
			}

			return result;
		}
		
	};

	protected static Route detectIntentKnow = new Route() {
		@Override
		public String handle(Request request, Response response) throws Exception {
			return detectIntentKnow(request, response);
		}
		
		private String detectIntentKnow(Request request, Response response) {
			response.type("application/json");

			DetectIntentKnowledge dk = new DetectIntentKnowledge();

			ObjectMapper mapper = new ObjectMapper();
			RequestPayload payload = null;
			try {
				payload = mapper.readValue(request.body(), RequestPayload.class);
			} catch (IOException e1) {
				parseFailResponse(response, e1.getMessage());
				return null;
			}

			if (payload == null || !payload.isValid()) {
				parseFailResponse(response, PARAMS_INVALIDS);
				return null;
			}
			
			String pathToKey = request.attribute("google_credentials_path");
			String result = null;

			try {
				result = dk.detectIntentKnowledge(payload.getProject_id(), payload.getKnow_base(), payload.getSession_id(),
						payload.getLang(), payload.getText(),pathToKey);
			} catch (Exception e) {
				parseFailResponse(response, e.getMessage());
				return null;
			}
			
			return result;
		}
	};

	public static final Route detectIntentRasa = new DetectTextRasa();
	
	private static void parseFailResponse(Response response, String message) {
		response.status(HttpStatus.SC_BAD_REQUEST);
		JsonObject obj = new JsonObject();
		obj.addProperty("fail", message);
		halt(HttpStatus.SC_INTERNAL_SERVER_ERROR, obj.toString());
	}
}
