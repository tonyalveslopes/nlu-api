package scio.integration.nlu.api.model;

import java.util.List;
import java.util.Map;

import com.google.cloud.dialogflow.v2beta1.KnowledgeAnswers.Answer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponsePayload {
	private String session_id;
	private String session_path;
	private String query_text;
	private String intent;
	private double confident;
	private String fullfilment_text;
	private Map<String, String> entities;
	private List<Answer> answers;
	
}
