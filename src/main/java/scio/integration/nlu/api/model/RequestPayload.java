package scio.integration.nlu.api.model;

import java.util.UUID;

import org.apache.logging.log4j.util.Strings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPayload {
	private String project_id;
	private String text;
	private String lang;
	private String know_base;
	private String model;
	
	private String session_id = UUID.randomUUID().toString();
	
	public String getLang() {
		if(Strings.isBlank(lang))
			lang = "pt-br";
		return lang;
	}
	
	public boolean isValid() {
		return ! (Strings.isBlank(project_id) || Strings.isBlank(text));
	}
}
