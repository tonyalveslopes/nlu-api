package scio.integration.nlu.api.util;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class SendHttpRequest {

	public static String sendRequisition(String body, String url) {
		HttpClient httpClient = HttpClient.newBuilder().version(Version.HTTP_2).build();
		HttpRequest mainRequest = HttpRequest.newBuilder()
				.uri(URI.create(url))
				.header("Content-Type", "application/json")
				.POST(BodyPublishers.ofByteArray(body.getBytes()))
				.build();
		String result = null;
		try {
			HttpResponse<String> mainResponse = httpClient.send(mainRequest, BodyHandlers.ofString());
			result = mainResponse.body() +"";
			
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
