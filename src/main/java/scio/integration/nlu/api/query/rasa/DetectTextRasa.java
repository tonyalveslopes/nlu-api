package scio.integration.nlu.api.query.rasa;

import static spark.Spark.halt;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.JsonObject;

import scio.integration.nlu.api.model.RequestPayload;
import scio.integration.nlu.api.model.ResponsePayload;
import scio.integration.nlu.api.util.SendHttpRequest;
import spark.Request;
import spark.Response;
import spark.Route;

public class DetectTextRasa implements Route {

	private static final String URL_BASE = "http://localhost:5000/parse";
	private static final String PARAMS_INVALIDS = "Params are not valids.";
	private static Logger log = LogManager.getLogger("DetectTextRasa");
	
	@Override
	public String handle(Request request, Response response) {
		response.type("application/json");
		ObjectMapper mapper = new ObjectMapper();
		RequestPayload payload = null;
		try {
			payload = mapper.readValue(request.body(), RequestPayload.class);
		} catch (IOException e1) {
			log.catching(e1);
			parseFailResponse(response, e1.getMessage());
			return null;
		}

		if (payload == null || ! payload.isValid()) {
			parseFailResponse(response, PARAMS_INVALIDS);
			return null;
		}
		
		String result = null;
		try {
			result = detect(payload.getText(), payload.getProject_id(), payload.getModel());
		} catch (Exception e) {
			parseFailResponse(response, e.getMessage());
		}

		return result;
	}
	
	private String detect(String text, String project, String model) throws IOException {
		JsonObject request = new JsonObject();
		
		request.addProperty("project", project);
		request.addProperty("q", text);
		request.addProperty("model", model);
		var result = SendHttpRequest.sendRequisition(request.toString(), URL_BASE);
		ObjectMapper mapper = new ObjectMapper();
		var jsonResult = mapper.readTree(result);
		var mapEntities = extractEntities(jsonResult);
		
		ResponsePayload response = ResponsePayload.builder()
				.intent(jsonResult.get("intent").get("name").asText())
				.confident(jsonResult.get("intent").get("confidence").asDouble())
				.query_text(jsonResult.get("text").asText())
				.entities(mapEntities)
				.build();
		
		ObjectMapper mapperResponse = new ObjectMapper();
		mapperResponse.enable(SerializationFeature.INDENT_OUTPUT);
		mapperResponse.setSerializationInclusion(Include.NON_NULL);
		StringWriter sw = new StringWriter();
		mapperResponse.writeValue(sw, response);
		
		return sw.toString();
		
	}

	private Map<String, String> extractEntities(JsonNode jsonResult) throws IOException {
		
		var entitiesList = jsonResult.findValues("entities");
		var mapEntities = new HashMap<String, String>();
		if(!entitiesList.isEmpty()) {
			var entities = entitiesList.get(0);
			for(int i =0 ; i < entities.size(); i++) {
				var entity = entities.get(i);
				var key = entity.get("entity").asText();
				var value = entity.get("value").asText();
				
				mapEntities.put(key, value);
			}
		}
		return mapEntities;
	}

	private static void parseFailResponse(Response response, String message) {
		response.status(HttpStatus.SC_BAD_REQUEST);
		JsonObject obj = new JsonObject();
		obj.addProperty("fail", message);
		halt(HttpStatus.SC_INTERNAL_SERVER_ERROR, obj.toString());
	}
 }
