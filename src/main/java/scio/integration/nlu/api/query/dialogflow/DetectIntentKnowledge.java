/*
  Copyright 2018, Google, Inc.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package scio.integration.nlu.api.query.dialogflow;

import java.io.StringWriter;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
// [START dialogflow_import_libraries]
// Imports the Google Cloud client library
import com.google.cloud.dialogflow.v2beta1.DetectIntentRequest;
import com.google.cloud.dialogflow.v2beta1.DetectIntentResponse;
import com.google.cloud.dialogflow.v2beta1.KnowledgeAnswers;
import com.google.cloud.dialogflow.v2beta1.KnowledgeBaseName;
import com.google.cloud.dialogflow.v2beta1.QueryInput;
import com.google.cloud.dialogflow.v2beta1.QueryParameters;
import com.google.cloud.dialogflow.v2beta1.QueryResult;
import com.google.cloud.dialogflow.v2beta1.SessionName;
import com.google.cloud.dialogflow.v2beta1.SessionsClient;
import com.google.cloud.dialogflow.v2beta1.TextInput;
import com.google.cloud.dialogflow.v2beta1.TextInput.Builder;

import scio.integration.nlu.api.http.GoogleCredentialsInit;
import scio.integration.nlu.api.model.ResponsePayload;

// [END dialogflow_import_libraries]

/** DialogFlow API Detect Intent sample with querying knowledge connector. */
public class DetectIntentKnowledge {

	// [START dialogflow_detect_intent_knowledge]
	/**
	 * Returns the result of detect intent with text as input.
	 *
	 * <p>
	 * Using the same `session_id` between requests allows continuation of the
	 * conversation.
	 *
	 * @param projectId       Project/Agent Id.
	 * @param knowledgeBaseId Knowledge base Id.
	 * @param sessionId       Identifier of the DetectIntent session.
	 * @param languageCode    Language code of the query.
	 * @param texts           The texts to be processed.
	 * @throws Exception
	 */
	public String detectIntentKnowledge(String projectId, String knowledgeBaseId, String sessionId,
			String languageCode, String text, String pathToKey) throws Exception {
		ResponsePayload payload =  null;
		// Instantiates a client
		try (SessionsClient sessionsClient = SessionsClient.create(GoogleCredentialsInit.authExplicitBeta(pathToKey))) {
			// Set the session name using the sessionId (UUID) and projectID (my-project-id)
			SessionName session = SessionName.of(projectId, sessionId);
			// Detect intents for each text input
			// Set the text and language code (en-US) for the query
			Builder textInput = TextInput.newBuilder().setText(text).setLanguageCode(languageCode);
			// Build the query with the TextInput
			QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();

			KnowledgeBaseName knowledgeBaseName = KnowledgeBaseName.of(projectId, knowledgeBaseId);
			QueryParameters queryParameters = QueryParameters.newBuilder()
					.addKnowledgeBaseNames(knowledgeBaseName.toString()).build();

			DetectIntentRequest detectIntentRequest = DetectIntentRequest.newBuilder().setSession(session.toString())
					.setQueryInput(queryInput).setQueryParams(queryParameters).build();
			// Performs the detect intent request
			DetectIntentResponse response = sessionsClient.detectIntent(detectIntentRequest);

			// Display the query result
			QueryResult queryResult = response.getQueryResult();
			KnowledgeAnswers knowledgeAnswers = queryResult.getKnowledgeAnswers();

			payload = ResponsePayload.builder().session_id(session.getSession()).session_path(session.toString())
					.query_text(queryResult.getQueryText()).intent(queryResult.getIntent().getDisplayName())
					.confident(queryResult.getIntentDetectionConfidence())
					.fullfilment_text(queryResult.getFulfillmentText())
					.answers(knowledgeAnswers.getAnswersList()).build();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.setSerializationInclusion(Include.NON_NULL);
		StringWriter sw = new StringWriter();
		mapper.writeValue(sw, payload);
		
		return sw.toString();
	}
}
