package scio.integration.nlu.api.query.dialogflow;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.cloud.dialogflow.v2.Context;
import com.google.cloud.dialogflow.v2.DetectIntentResponse;
import com.google.cloud.dialogflow.v2.QueryInput;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.cloud.dialogflow.v2.SessionsClient;
import com.google.cloud.dialogflow.v2.TextInput;
import com.google.cloud.dialogflow.v2.TextInput.Builder;

import scio.integration.nlu.api.http.GoogleCredentialsInit;
import scio.integration.nlu.api.model.ResponsePayload;

public class DetectIntentTexts {

	/**
	 * Returns the result of detect intent with texts as inputs.
	 *
	 * Using the same `session_id` between requests allows continuation of the
	 * conversation.
	 * 
	 * @param projectId    Project/Agent Id.
	 * @param texts        The text intents to be detected based on what a user
	 *                     says.
	 * @param sessionId    Identifier of the DetectIntent session.
	 * @param languageCode Language code of the query.
	 * @throws IOException
	 */
	public String detectIntentTexts(String projectId, String text, String sessionId, String languageCode,
			String pathToKey) throws IOException {
		ResponsePayload payload = null;
		// Instantiates a client
		try (SessionsClient sessionsClient = SessionsClient
				.create(GoogleCredentialsInit.authExplicit(pathToKey, "session_settings"))) {
			// Set the session name using the sessionId (UUID) and projectID (my-project-id)
			SessionName session = SessionName.of(projectId, sessionId);

			// Detect intents for each text input
			Builder textInput = TextInput.newBuilder().setText(text).setLanguageCode(languageCode);
			QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();

			DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);
			
			QueryResult queryResult = response.getQueryResult();
			var contexts = queryResult.getOutputContextsList();
			var mapEntities = extractEntities(contexts);

			payload = ResponsePayload.builder().session_id(session.getSession()).session_path(session.toString())
					.query_text(queryResult.getQueryText()).intent(queryResult.getIntent().getDisplayName())
					.confident(queryResult.getIntentDetectionConfidence())
					.fullfilment_text(queryResult.getFulfillmentText())
					.entities(mapEntities).build();

			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.setSerializationInclusion(Include.NON_NULL);
			StringWriter sw = new StringWriter();
			mapper.writeValue(sw, payload);

			return sw.toString();
		}

	}

	private HashMap<String,String> extractEntities(List<Context> contexts) {
		var mapEntities = new HashMap<String, String>();
		
		contexts.stream().forEach(ctx -> ctx.getParameters().getFieldsMap().entrySet().stream().filter(map -> !map.getValue().getStringValue().isBlank())
				.forEach(map -> 
					mapEntities.put(map.getKey(), map.getValue().getStringValue()) 
				));
		
		return mapEntities;
	}
}